import serial
import cv2
import time
import PoseModule as pm


#cap = cv2.VideoCapture("testBody.mp4")
cap = cv2.VideoCapture(0)

#init values
pTime =0
u16AngleLeft =0
u16AngleLeftPrev = 0
u16AngleRight =0
u16AngleRightPrev = 0
#value default servos arm
ConvDataRight = 1200
ConvDataLeft = 2260

detector = pm.poseDetector()

#config UART serial Port
ser = serial.Serial(
    port='/dev/serial0',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)
if ser.isOpen():
    ser.close()
ser.open()
ser.isOpen()

while True:
    success, img = cap.read()

    img = cv2.rotate(img, cv2.ROTATE_180)
    
    img = detector.findPose(img, False)
    
    
    lmList = detector.findPosition(img, draw = False)
    
    if len(lmList) !=0:
        
        #Number of values, more information https://google.github.io/mediapipe/solutions/pose.html
        #left arm
        angleLeft = detector.findAngle(img, 13,11, 23)
        
        #right arm
        angleRight = detector.findAngle(img, 24,12, 14)

        u16AngleRight = int(angleRight)
        u16AngleLeft = int(angleLeft)
        
        if u16AngleRight != u16AngleRightPrev or u16AngleLeft != u16AngleLeftPrev:
            #limit to don't damage the robot
            if u16AngleRight < 20:
                u16AngleRight = 20
            if u16AngleRight > 130:
                u16AngleRight = 130
                
            if u16AngleLeft < 20 :
                u16AngleLeft = 20
            if u16AngleLeft > 130 :
                u16AngleLeft = 130
            
            #compute the value to move the servo
            ConvDataRight = int((u16AngleRight*11.8) + 963.6)  
            ConvDataLeft = int((u16AngleLeft*(-16))+2580)
            
            
            #to send data to Servoontroller 
#             ser.write("#31P".encode())
#             ser.write(str(ConvDataRight).encode())
#             ser.write("#2P".encode())
#             ser.write(str(ConvDataLeft).encode())
#             ser.write("T100D100\r\n".encode())
            
        u16AngleRightPrev = u16AngleRight
        u16AngleLeftPrev = u16AngleLeft
    
    #cTime = time.time()
    #fps = 1/(cTime-pTime)
    #pTime = cTime
    
    #cv2.putText(img, str(int(fps)),(78,50), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 0), 3)
    
    #Show the video
    cv2.imshow("Image",img)

    cv2.waitKey(1)
